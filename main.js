const { google } = require('googleapis');
const keys = require('./keys.json');
const excelJs = require('exceljs');


const client = new google.auth.JWT(
    keys.client_email,
    null,
    keys.private_key,
    ['https://www.googleapis.com/auth/spreadsheets']
);

client.authorize((err, tokens) => {

    if (err) {
        console.log(err);
        return;
    } else {
        console.log('Connected');
        gsrun(client)
    }

});

async function gsrun(cl){

    const gsapi = google.sheets({version: 'v4', auth: cl});

    const wb = new excelJs.Workbook();
    let excelFile = await wb.xlsx.readFile('DataSchema.xlsx');
    let ws = excelFile.getWorksheet('Data');
    let data = ws.getSheetValues();
    data = data.map((r) => {
        return [r[1], r[2].toLocaleString('en-US', { month: 'numeric', day: 'numeric', year: 'numeric' }), r[3], 
        r[4], r[5], r[6], r[7], r[8], 
        r[9], 
        r[10], 
        r[11], r[12]]
    });
    data.shift();
    data.shift();
    
    const updateOptions = {
        spreadsheetId: '1d0Pjk6H_nXFNG17YexyDcCQgX8M3zppM6wuMv_tV1DU',
        range: 'Excel Data!A2',
        valueInputOption: 'USER_ENTERED',
        resource: { values: data }
    };

    let res = await gsapi.spreadsheets.values.update(updateOptions);
    console.log(res);

    // const opt = {
    //     spreadsheetId: '1d0Pjk6H_nXFNG17YexyDcCQgX8M3zppM6wuMv_tV1DU',
    //     range: 'Data!A2:B5'
    // };

    // let data = await gsapi.spreadsheets.values.get(opt);
    // let dataArray = data.data.values;

    // dataArray = dataArray.map((r) => {
    //     while(r.length < 2) {
    //         r.push('');
    //     }
    //     return r;
    // });

    // console.log(dataArray)

    // let newDataArray = dataArray.map((r) => {
    //     r.push(r[0] + '-' + r[1]);
    //     return r;
    // });
    // console.log(newDataArray);


}